In order to compile TolRInside we have to install previously the R packages Rcpp and RInside.

We provide a specific source code for RInside because we want to turn on the CALLBACK functionality which comes commented by default

#define RINSIDE_CALLBACKS

To install Rcpp and RInside first download the source code of the package from tol-project:

* download  http://packages.tol-project.org/win32/development/Rcpp_0.11.4.4.tar.gz
* download http://packages.tol-project.org/win32/development/RInside_0.2.12.tar.gz

Then execute the corresponding INSTALL 

* R CMD INSTALL Rcpp_0.11.4.4.tar.gz
* R CMD INSTALL RInside_0.2.12.tar.gz