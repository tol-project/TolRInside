#ifndef __TolR_h__
#define __TolR_h__

#include "RInside.h"

namespace TolR {

Rcpp::Environment::Binding Access( const std::string& name );
RInside::Proxy ParseEval( const std::string & line );

}

#endif
