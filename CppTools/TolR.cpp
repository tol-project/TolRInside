#include "TolR.h"
#include "tol/tol_bout.h"
#include <sstream>

namespace TolR {

// parse complete line, return in ans; error code rc
int ParseEval( const std::string &line, SEXP &ans )
{
  ParseStatus status;
  SEXP cmdSexp, cmdexpr = R_NilValue;
  int i, errorOccurred;
  stringstream ss;

  Rcpp::Environment glob_env = Rcpp::Environment::global_env( );
  PROTECT( cmdSexp = Rf_allocVector( STRSXP, 1 ) );
  SET_STRING_ELT( cmdSexp, 0, Rf_mkChar( line.c_str( ) ) );

  cmdexpr = PROTECT( R_ParseVector( cmdSexp, -1, &status, R_NilValue ) );

  switch( status )
    {
    case PARSE_OK:
      // Loop is needed here as EXPSEXP might be of length > 1
      for( i = 0; i < Rf_length( cmdexpr ); i++ )
        {
        ans = R_tryEval( VECTOR_ELT( cmdexpr, i ), glob_env, &errorOccurred);
        if ( errorOccurred ) 
          {
          ss << "Error in evaluating R code (" << status << ")"; 
          Warning( ss.str( ).c_str() );
          UNPROTECT(2);
          return 1;
          }
        }
      break;
    case PARSE_INCOMPLETE:
      // need to read another line
      Warning( "Incomplete R expression" );
      UNPROTECT(2);
      return 1;
    case PARSE_NULL:
      ss << "ParseStatus is null (" << status << ")";
      Warning( ss.str( ).c_str() );
      UNPROTECT(2);
      return 1;
      break;
    case PARSE_ERROR:
      ss << "Parse Error: \"" << line << "\"";
      Warning( ss.str( ).c_str() );
      UNPROTECT(2);
      return 1;
      break;
    case PARSE_EOF:
      ss << "ParseStatus is eof (" << status << ")";
      Warning( ss.str( ).c_str() );
      break;
    default:
      ss << "ParseStatus is not documented (" << status << ")";
      Warning( ss.str( ).c_str() );
      UNPROTECT(2);
      return 1;
    }
    UNPROTECT(2);
    return 0;
}

RInside::Proxy ParseEval( const std::string & line ) {
  SEXP ans;
  int rc = ParseEval( line, ans );
  if (rc != 0) 
    {
    throw std::runtime_error(std::string("Error evaluating: ") + line);
    }
  return RInside::Proxy( ans );
}

Rcpp::Environment::Binding Access( const std::string& name )
{
  Rcpp::Environment glob_env = Rcpp::Environment::global_env( );
  return glob_env[ name ];
}

}
