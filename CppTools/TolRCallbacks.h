#ifndef __TolRCallbacks_h__
#define __TolRCallbacks_h__
#include <RInside.h>

class TolRCallbacks 
#if defined(RINSIDE_CALLBACKS)
: public Callbacks
#endif
{
public:
  virtual void WriteConsole( const std::string& line, int type );
  virtual bool has_WriteConsole()
  {
    return true;
  }
};

#endif
