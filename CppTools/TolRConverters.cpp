#include "tol/tol_bdatgra.h"
#include "tol/tol_btxtgra.h"
#include "tol/tol_blanguag.h"
#include "tol/tol_bdtegra.h"
#include <tol/tol_bmatgra.h>
#include "TolRConverters.h"
#include <Rcpp.h>
#include <RInside.h>

// define template specialisations for as and wrap

namespace Rcpp {
  // BDat
  template <> BDat as(SEXP xR)
  {
    double x = as<double>(xR);
    return BDat( x );
  }

  template <> SEXP wrap(const BDat &xTol)
  {
    return Rcpp::wrap( xTol.GetValue( ) ); 
  }

  // BDate
  template <> BDate as(SEXP dR)
  {
    Rcpp::Date d( dR );
    return BDate( d.getYear( ), d.getMonth( ), d.getDay( ) );
  }

  template <> SEXP wrap(const BDate &dTol)
  {
    return Rcpp::wrap( Rcpp::Date( dTol.Year( ), dTol.Month( ), dTol.Day( ) ) );
  }

  // BText
  template <> BText as(SEXP txtR)
  {
    std::string r = as<std::string>( txtR );
    return BText( r.c_str( ) );
  }

  template <> SEXP wrap(const BText &txtTol)
  {
    return Rcpp::wrap( Rcpp::String( txtTol.String( ) ) );
  }

  // BMatrix
  template <> BMat as(SEXP matR)
  {
    BMat matTol;
    
    AsBMat( matR, matTol );
    return matTol;
  }

  template <> SEXP wrap(const BMat &matTol)
  {
    Rcpp::NumericMatrix matR( matTol.Rows( ), matTol.Columns( ) );
    for ( int r = 0; r < matTol.Rows( ); ++r )
      {
      for ( int c = 0; c < matTol.Columns( ); ++c )
        {
        matR( r, c ) = matTol( r, c ).GetValue();
        }
      }
    
    return Rcpp::wrap( matR );
  }

  void AsBMat(SEXP matR, BMat &matTol)
  {
    Rcpp::NumericMatrix _matR( matR );
    matTol.Alloc( _matR.nrow( ), _matR.ncol( ) );
    for ( int r = 0; r < _matR.nrow( ); ++r )
      {
      for ( int c = 0; c < _matR.ncol( ); ++c )
        {
        matTol( r, c ) = _matR( r, c );
        }
      }
  }

  // BSyntaxObject
  template <> BSyntaxObject* as(SEXP x)
  {
    //std::cout << "x = " << x << std::endl;
    //Rf_PrintValue( x );
    if ( Rcpp::is<NumericMatrix>( x ) )
      {
      BMat mat;
      Rcpp::AsBMat( x, mat );
      return new BContensMat( "", mat );
      }
    if ( Rcpp::is<std::string>( x ) )
      {
      BText txt = Rcpp::as<BText>( x );
      return new BContensText( "", txt );
      }
    if ( Rcpp::is<Rcpp::Date>( x ) )
      {
      BDate dte( Rcpp::as<BDate>( x ) );
      return new BContensDate( "", dte );
      }
    if ( Rf_length( x ) == 1 && Rf_isNumeric( x ) )
      {
      double _x =  as<double>( x );
      return new BContensDat("", _x );
      }
    return NULL;
  }

  template <> SEXP wrap(const BSyntaxObjectPtr& x)
  {
    if ( x->Grammar( ) == GraReal( ) )
      {
      BDat & _x = Dat( x );
      return wrap<BDat>( _x );  
      }
    if ( x->Grammar( ) == GraText( ) )
      {
      BText & _x = Text( x );
      return wrap<BText>( _x );  
      }
    if ( x->Grammar( ) == GraDate( ) )
      {
      BDate & _x = ::Date( x );
      return wrap<BDate>( _x );  
      }
    if ( x->Grammar( ) == GraMatrix( ) )
      {
      BMat & _x = Mat( x );
      return wrap<BMat>( _x );  
      }
    return R_NilValue;
  }
}
