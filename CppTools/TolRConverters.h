#ifndef __TolRConverters_h__

#include <RcppCommon.h>

#include "tol/tol_bdat.h"
#include "tol/tol_bdate.h"
#include "tol/tol_btext.h"
#include "tol/tol_bmatimp.h"
#include "tol/tol_bsyntax.h"

// non-intrusive extension via template specialisation
typedef BSyntaxObject* BSyntaxObjectPtr;

namespace Rcpp {
  
  // BDat
  template <> BDat as(SEXP x);
  template <> SEXP wrap(const BDat &x);

  // BDate
  template <> BDate as(SEXP d);
  template <> SEXP wrap(const BDate &d);

  // BText
  template <> BText as(SEXP t);
  template <> SEXP wrap(const BText &t);

  // BMatrix
  template <> BMat as(SEXP matR);
  template <> SEXP wrap(const BMat &matTol);
  void AsBMat(SEXP matR, BMat &matTol);
  
  // BSyntaxObject
  template <> BSyntaxObjectPtr as(SEXP x);
  template <> SEXP wrap(const BSyntaxObjectPtr& x);
}
#endif

