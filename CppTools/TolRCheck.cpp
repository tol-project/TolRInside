#define LOCAL_NAMEBLOCK local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bdatgra.h>
#include <ltdl.h>

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& LOCAL_NAMEBLOCK = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockTolRCheck( )
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = LOCAL_NAMEBLOCK;
  return( copy );
}

//---------------------------------------------------------------------------
DeclareContensClass(BDat, BDatTemporary, BDatIsRLoaded);
DefMethod(1, BDatIsRLoaded, "IsRLoaded", 1, 1, "Real", "(Real void)",
          "",
  BOperClassify::System_);
//----------------------------------------------------------------------------
void BDatIsRLoaded::CalcContens()
{
  lt_dlhandle h = lt_dlopen( NULL );
  //printf( "h = %p\n", h );
  if ( h )
    {
    contens_ = BDat( lt_dlsym( h, "R_NilValue" ) != NULL );
    }
}
