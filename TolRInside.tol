/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : TolRInside.tol
// PURPOSE: Defines package TolRInside
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
NameBlock TolRInside = 
//////////////////////////////////////////////////////////////////////////////
[[
  Text _.autodoc.name = "TolRInside";
  Text _.autodoc.brief = "Embed an R Interpreter";
  Text _.autodoc.description = "Functions to evaluate R code from TOL.";
  Text _.autodoc.url = 
    "http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
  Set _.autodoc.keys = [["R","embed"]];
  Set _.autodoc.authors = [["josp@tol-project.org", "pgea@bayesforecast.com"]];
  Text _.autodoc.minTolVersion = "v3.3";
  Text _.autodoc.platform = 
    TolPackage::TolPlatform::ObtainPlatform("OWN", "OWN", "GNU");
  Text _.autodoc.variant = 
    TolPackage::TolPlatform::ObtainVariant(_.autodoc.platform);
  Real _.autodoc.version.high = 2;
  Real _.autodoc.version.low = 1;
  Set _.autodoc.dependencies = Copy(Empty);
  Set _.autodoc.nonTolResources = [[
    TolPackage::TolPlatform::ObtainDllFolder(?)
  ]];
  Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

  NameBlock CppTools = [[ Real unused = ? ]];
  NameBlock CppTools_Embedded = [[ Real unused = ? ]];
  NameBlock CppTools_Check = [[ Real unused = ? ]];

  NameBlock API = [[
    #Embed "functions/fun.tol";
    #Embed "functions/get.tol"
  ]];

  #Embed "functions/stack.tol";
  #Embed "functions/rchecks.tol";
  
  ////////////////////////////////////////////////////////////////////////////
  Real _CheckRHome(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text rHome = _GetRHome(?);
    Case(TextLength(rHome)==0, {
      WriteLn("Para poder usar TolRInside ha de existir la variable \n"
        "  de entorno R_HOME apuntando al directorio de instalaci�n de R.\n"
        "  Por ejemplo: \"C:/Program Files/R/R-3.2.0/i386\"", "E");
      False
    }, Not(DirExist(_GetRBin(?))), {
      WriteLn("La variable de entorno R_HOME no es correcta.\n"
        "  R_HOME="<<rHome<<"\n"
        "  Ha de apuntar al directorio de instalaci�n de R.\n"
        "  Por ejemplo: \"C:/Program Files/R/R-3.2.0\"", "E");
      False
    }, Not(RcheckPackage("Rcpp") ), {
      WriteLn("Es necesario tener instalado en R el paquete Rcpp.", "E");
      False
    }, Not(RcheckPackage("RInside") ), {
      WriteLn("Es necesario tener instalado en R el paquete RInside.", "E");
      False
    }, True, {
      If(Not(RcheckPackage("tolRlink")), {
        WriteLn("Para poder aprovechar todas las funcionalidades "
          "de TolRInside\n  es conveniente tener instalado en R el "
          "paquete tolRlink.", "W");
        True
      }, True)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text DllFile(Text name)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text _name = If(name=="", Name(_this), name);
    Text dllPath.rel = GetAbsolutePath(
      TolPackage::TolPlatform::ObtainDllFolder(?))+"/";
    dllPath.rel+_name+"."+GetSharedLibExt(0)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real _PrependPath(Text path)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(OSName=="WINDOWS", {
      // Necesito Tcl
      Real If(Not(Tcl_Eval("info tclversion")[2]), 
        Eval("Real {#Require TclCore; 1};"));
      // Obtiene la variable PATH actual
      Text oldPath = Replace(Tcl_Eval("set ::env(PATH)")[1], "\\", "/");
      // Obtiene el nuevo directorio y se a�ade a la variable PATH
      Text addPath = Replace(GetFilePath(GetAbsolutePath(path)), "\\", "/");
      Text newPath = addPath<<";"<<oldPath;
      // Establece el nuevo PATH
      Tcl_Eval("set ::env(PATH) "<<Qt(Replace(newPath, "/", "\\\\")))[2]
    }, 0)
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Real _is_started = False;
  Text _dllTolRInside = "";
  Text _dllTolREmbedded = "";
  Text _dllTolRCheck = "";
  ////////////////////////////////////////////////////////////////////////////
  Real StartActions(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_is_started, False, {
      Real _is_started := True;
      Set plat = PlatformInfo(?);
      Real r_OK = _CheckRHome(?);
      If(r_OK, {
        //--------------------------------------------------------------------
        // Chequeo si estoy dentro de R
        Text _dllTolRCheck := DllFile("TolRCheck");
        NameBlock CppTools_Check := LoadDynLib(_dllTolRCheck);
        Real rLoaded = CppTools_Check::IsRLoaded(?);
        Real If(Not(rLoaded), {
          If(plat::SystemName=="Windows", {
            // Se carga la librer�a de R y se a�ade su directorio 
            // a la variable de entorno PATH.
            // Es necesario para la carga de las DLL's y el correcto 
            // funcionamiento de algunos paquetes de R.
            Text rDynLibPath = _GetRDynLibPath(?);
            Real _PrependPath(rDynLibPath);
            Real LoadDynLib(rDynLibPath);         
            // Windows x64 tambi�n necesita cargar Rlapack.dll
            Real If(plat::PointerSize==8, {
              Text rDynLibPath2 = Replace(rDynLibPath, "R.dll", "Rlapack.dll");
              LoadDynLib(rDynLibPath2)
            }, 1)
          }, {
            // No tengo claro si esto hay que hacerlo en Windows tambi�n.
            Text rInsideDir = _RGetPackageDir("RInside");
            Text libRInside = rInsideDir + "/lib/libRInside.so";
            DynLoad(libRInside, { [[Real global = 1]] })
          });
          // Se carga la librer�a encargada de inicializar R
          Text _dllTolREmbedded := DllFile("TolREmbedded");
          NameBlock CppTools_Embedded := LoadDynLib(_dllTolREmbedded);
        1});
        // Se carga la librer�a propia del paquete
        Text _dllTolRInside := DllFile("");
        NameBlock CppTools := LoadDynLib(_dllTolRInside);
        //--------------------------------------------------------------------
        // A partir de aqu� ya podemos evaluar expresiones R
        Real CppTools::NewEnv("tolRI");
        Real UsingNameBlock(API);
        Code PutDescription(I2(
          "Evaluates a R expression and obtains the corresponding variable.",
          "Evalua una expresi�n en R y obtiene la correspondiente variable."
        ), API::REval);
        Code PutDescription(I2(
          "Evaluates an expression in R.",
          "Evalua una expresi�n en R."
        ), API::RCall);
        Code PutDescription(I2(
          "Loads a package in R.",
          "Carga un paquete en R."
        ), API::RLibrary);
        Code PutDescription(I2(
          "Executes a R function with TOL arguments.",
          "Ejecuta una funci�n de R con argumentos de TOL."
        ), API::RExecute);
        //--------------------------------------------------------------------
        True
      }, False)
    })
  }
]];

//////////////////////////////////////////////////////////////////////////////

